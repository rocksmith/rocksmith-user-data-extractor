const args = process.argv.slice(2);
let infile = args[0];

const fs = require('fs')
const aesjs = require('aes-js')
const { unzipSync } = require('node:zlib')
const key = Buffer.from('\x72\x8B\x36\x9E\x24\xED\x01\x34\x76\x85\x11\x02\x18\x12\xAF\xC0\xA3\xC2\x5D\x02\x06\x5F\x16\x6B\x4B\xCC\x58\xCD\x26\x44\xF2\x9E', 'ascii')

let saveFile = fs.readFileSync(infile, { encoding: null })

saveFile = saveFile.slice(20)

const aesEcb = new aesjs.ModeOfOperation.ecb(key)
const decrypted = aesEcb.decrypt(saveFile)

const unzipped = unzipSync(decrypted)
const text = unzipped.toString('utf8').slice(0, -1)

const json = JSON.parse(text)

console.log(JSON.stringify(json, null, 4))