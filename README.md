# rocksmith-user-data-extractor

Javascript unpacker for encrypted Rocksmith user data files

# Usage

```
rocksmith_user_data_extractor.exe <input-file>
```

# Build Requirements

Install Node.js (https://nodejs.org/en/)

Install pkg
```
npm install -g pkg
```

# Build Instructions

```
build.bat
```
